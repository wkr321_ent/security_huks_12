/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HKS_BMS_API_WRAP_H
#define HKS_BMS_API_WRAP_H

#include "hks_type_inner.h"

#ifdef __cplusplus
extern "C" {
#endif

#define HAP_NAME_LEN_MAX 128

int32_t HksGetHapInfo(const struct HksProcessInfo *processInfo, struct HksBlob *hapInfo);

int32_t HksGetHapName(int32_t tokenId, int32_t userId, char *hapName, int32_t hapNameSize);

#ifdef __cplusplus
}
#endif

#endif // HKS_BMS_API_WRAP_H